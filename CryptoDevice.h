#pragma once

#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#include "des.h"
#include <exception>
#include <md5.h>
#include "hex.h"
#include "rsa.h"

class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);
	std::string hashMD5(std::string);

private:
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
	CryptoPP::byte digest[CryptoPP::Weak1::MD5::DIGESTSIZE]; 
};
