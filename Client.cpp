#include "Client.h"



std::map<std::string, CryptoPP::RSA::PublicKey> keys;

int process_client(client_type &new_client)
{
	CryptoDevice cryptoDevice;
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{
			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			std::string strMessage(new_client.received_message);

			size_t position = strMessage.find(": ") + 2;
			std::string prefix = strMessage.substr(0, position);
			std::string postfix = strMessage.substr(position);
			std::string decrypted_message;

			if (postfix == "Disconnected")
			{
				decrypted_message = postfix;
			}
			else
			{
				decrypted_message = cryptoDevice.decryptAES(postfix);
			}
			
			if (iResult != SOCKET_ERROR)
			{
				std::cout << prefix + decrypted_message << std::endl;
			}
			else
			{
				std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
		std::cout << "The server has disconnected" << std::endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	std::string line = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	std::string message;
	CryptoDevice cryptoDevice;

	std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		
		std::string username, password, output;
		std::string hashedName1 = "3A51D2DAE2A4C583A3F70CEED2E17FF9", hashedPassword1 = "010FB7A092ACE358DB343208A298C8EC";
		//for you; username = "Mor", password = "AdvancedProgramming"
		std::string hashedName2 = "CA96AD0A01FA5A7836CC872ECCED65F9", hashedPassword2 = "AFA3CFBA9D02314CFFCDC98452C0479B";
		//for me; username = "Itay", password = "JamesBald"
		bool login = false;

		for (int i = 0; i < 3 && !login; i++)
		{
			
			std::cout << "Register secretly!" << std::endl;
			getline(std::cin, line);
			size_t position = line.find(" ");
			std::string username = line.substr(0, position);
			std::string password = line.substr(position + 1);
			std::cout << "name:" << username << std::endl;
			std::cout << "password:" << cryptoDevice.hashMD5(password) << std::endl;
			if ((cryptoDevice.hashMD5(username) == hashedName1 && cryptoDevice.hashMD5(password) == hashedPassword1) ||
				(cryptoDevice.hashMD5(username) == hashedName2 && cryptoDevice.hashMD5(password) == hashedPassword2))
			{
				std::cout << "Agent accepted. welcome in!" << std::endl << "Successfully Connected" << std::endl;
				login = true;
			}
			else
			{
				std::cout << "Agent denied. try again" << std::endl;
			}
		}
		if (!login)
		{
			::closesocket(client.socket);
			::WSACleanup();
			return 2;
		}
		client.id = std::atoi(client.received_message);

		std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
			std::getline(std::cin, line);

			std::string cipher = cryptoDevice.encryptAES(line);

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		std::cout << client.received_message << std::endl;

	std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}